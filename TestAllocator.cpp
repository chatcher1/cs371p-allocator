// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    //using allocator_type = std::allocator<int>;
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        } // lil baby seg
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// uncomment
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    //using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    //using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}                                         // fix test


TEST(AllocatorFixture, chatcher1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 52;
    const value_type v = 'a';
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            char c = 0;
            while (p != e) {
                char val = v + (c%26);
                c++;
                x.construct(p, val);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), 2);
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, chatcher2) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 124;
    const value_type v = 1.0;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        x.deallocate(b, 124);

        ASSERT_EQ(*(reinterpret_cast<int *>(b) - 1), 992);
    }
}

TEST(AllocatorFixture, chatcher3) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 124;
    const value_type v = 1.0;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        x.deallocate(b, s);
        const pointer p = x.allocate(10);

        const pointer h = x.allocate(60);

        const pointer f = x.allocate(52);

        ASSERT_EQ(*(reinterpret_cast<int *>(f) - 1), -1*52*8);

    }
}

TEST(AllocatorFixture, chatcher4) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 1.0;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        for(int d = 0; d < 61; d++) {
            pointer p = x.allocate(s);
            if(d == 60) {
                x.deallocate(p, s);
                ASSERT_EQ(*(reinterpret_cast<int *>(p) - 1), 16);
            }
        }


    }
}

TEST(AllocatorFixture, chatcher5) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 1.0;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        for(int d = 0; d < 61; d++) {
            pointer p = x.allocate(s);
            if(d == 60) {
                x.deallocate(p, s);
                ASSERT_EQ(*(reinterpret_cast<int *>(p) - 1), 16);
            }
        }


    }
}

TEST(AllocatorFixture, chatcher6) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 1.0;
    const pointer    b = x.allocate(s);
    allocator_type::iterator it = x.begin();
    ASSERT_EQ(*it, -1*10*8);
}
TEST(AllocatorFixture, chatcher7) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 1.0;
    const pointer    b = x.allocate(s);
    allocator_type::iterator it = x.end();
    --it;
    ASSERT_EQ(*(it), 904);
    it++;
    it--;
    ASSERT_EQ(*(it), 904);

}

TEST(AllocatorFixture, chatcher8) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 1.0;
    pointer p;
    pointer q;
    for(int j = 0; j < 5; j++) {
        pointer tmp = x.allocate(s);
        q = tmp;
        pointer tmp2 = x.allocate(s*2);
        x.deallocate(tmp, s);
        p = tmp2;
    }
    int * casted = reinterpret_cast<int *>(p)-1;
    ASSERT_EQ(*casted, -1*2*s*8);

    ASSERT_EQ(q < p, true);


}

TEST(AllocatorFixture, chatcher9) {
    using allocator_type = my_allocator<char, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 100;
    const value_type v = 'a';
    const pointer    b = x.allocate(s);
    x.construct(b, v);

    ASSERT_EQ(*reinterpret_cast<char*>(b), 'a');

}

TEST(AllocatorFixture, chatcher10) {
    using allocator_type = my_allocator<char, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 100;
    const value_type v = 'a';
    pointer b = x.allocate(s);
    x.construct(b, v);

    b++;
    x.construct(b, 'b');

    ASSERT_EQ(reinterpret_cast<char*>(b)[0], 'b');

}
TEST(AllocatorFixture, chatcher11) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 100;
    const value_type v = 6;
    const pointer b = x.allocate(s);


    ASSERT_EQ(x[0], 100*4*-1);

}
TEST(AllocatorFixture, chatcher12) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 100;
    const value_type v = 6;
    const pointer b = x.allocate(s);



    allocator_type::iterator it = x.end();
    allocator_type::iterator it2 = x.end();

    ASSERT_EQ(*(--it), *(--it2));

}

TEST(AllocatorFixture, chatcher13) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 100;
    const value_type v = 6;
    const pointer b = x.allocate(s);



    allocator_type::iterator it = x.end();
    allocator_type::iterator it2 = x.end();

    ASSERT_NE(&it, &it2);

}
TEST(AllocatorFixture, chatcher14) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 124;
    const value_type v = 1.0;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        x.deallocate(b, s);
        const pointer p = x.allocate(10);

        const pointer h = x.allocate(60);

        const pointer f = x.allocate(52);


        x.deallocate(f, 52);

        ASSERT_EQ(*(reinterpret_cast<int *>(f) - 1), 52*8);
    }
}
TEST(AllocatorFixture, chatcher15) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 2;
    const value_type v = 1.0;
    const pointer    b = x.allocate(s);
    const pointer next = x.allocate(121);
    allocator_type::iterator c = x.end();
    c--;
    ASSERT_EQ(*c, 121*-1*8);
}


TEST(AllocatorFixture, chatcher16) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 2;
    const value_type v = 1.0;
    const pointer    b = x.allocate(5);
    const pointer next = x.allocate(1);
    const pointer d = x.allocate(1);
    const pointer e = x.allocate(1);
    const pointer f = x.allocate(1);
    x.deallocate(d, 1);
    x.deallocate(next, 1);
    x.deallocate(b, 5);
    const pointer g = x.allocate(7);
    ASSERT_EQ(g, b);
}