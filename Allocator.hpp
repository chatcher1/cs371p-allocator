// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& l, const iterator& r) {

            return l._p == r._p;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {

            return _p[0];
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            ++_p;

            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            --_p;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& l, const const_iterator& r) {
            return l._p == r._p;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {

            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {

            ++_p;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {

            --_p;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        if(N < sizeof(T) + (2* sizeof(int))) {
            return false;
        }
        /*checks that the sentinels are well-placed and correct*/
        const_iterator it = begin();
        const_iterator t = end();

        /*So that constructor is O(1)*/
        if(*it == N - 2*sizeof(int)) {
            int sentinel = *it;

            if(sentinel == 0) {
                return false;
            }
            --t;
            if(sentinel != *t) {
                return false;
            }
            return true;
        }


        while(it != t) {
            int sentinel = *it;

            if(sentinel == 0) {
                return false;
            }
            if(sentinel < 0) {

                int absSent = (sentinel * -1)/sizeof(int);

                while(absSent >= 0) {
                    ++it;
                    absSent--;
                }

                if(*it != sentinel) {

                    return false;
                }
                ++it;
            }
            else {

                int ind = sentinel/sizeof(int);

                while(ind >= 0) {
                    ++it;
                    ind--;
                }

                if(*it != sentinel) {

                    return false;
                }
                ++it;

                if(it != t) {

                    if(*it > 0) {

                        return false;
                    }
                }
            }
        }

        // <use iterators>

        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        /*
            casts the char * to an int * then sets first and last indices to
            sentinel (N-2*sizeof(int)) to show how much space is free
        */

        if(N < sizeof(T) + 2*(sizeof(int))) {
            throw std::bad_alloc();
        }
        int * con = reinterpret_cast<int *>(a);
        con[0] = N - 2*sizeof(int);
        con[N/sizeof(int)-1] = N - 2*sizeof(int);
        assert(valid());

    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type sizeSpace) {
        int * intAdj = reinterpret_cast<int *>(a);

        int * intEnd = reinterpret_cast<int*>(&a[N]);

        while(intAdj != intEnd) {

            int available = *intAdj;
            /*
            If a busy block skip over block.
            If don't have enough space, skip over a free block.
            If have extra room (enough for a new free block), divide up current free block into
            busy and free block.
            If have not enough room for a new free block, make the whole block a busy block.
            */

            if(available < 0) {
                available*=-1;
                available/=sizeof(int);
                while(available >= -1) {
                    ++intAdj;
                    available--;
                }

            }
            else if(available < sizeSpace*sizeof(T)) {

                available/=sizeof(int);
                while(available >= -1) {
                    ++intAdj;
                    available--;
                }
            }
            else {
                pointer p = reinterpret_cast<T*>(intAdj+1);

                if((sizeSpace+1) * sizeof(T) + 2*sizeof(int)  <= available) {
                    *intAdj = -1 * (int)(sizeSpace) * sizeof(T);
                    int amountIn = (int)(sizeSpace) * (sizeof(T)/sizeof(int));

                    ++intAdj;
                    while(amountIn > 0) {
                        ++intAdj;
                        amountIn--;

                    }

                    *intAdj = -1 * (int)(sizeSpace) * sizeof(T);
                    ++intAdj;
                    if(intAdj != intEnd) {
                        available-=((sizeSpace * sizeof(T) + 2*sizeof(int)));
                        *intAdj = available;
                        int iter = available/sizeof(int);

                        while(iter >= 0) {
                            ++intAdj;
                            iter--;
                        }

                        *intAdj = available;
                        intAdj++;
                    }
                }
                else {
                    *intAdj = -1 * available;
                    int amountIn = available/sizeof(int);
                    ++intAdj;

                    while(amountIn > 0) {
                        ++intAdj;
                        amountIn--;
                    }
                    *intAdj = -1 * available;
                    ++intAdj;

                }
                assert(valid());

                return p;
            }
        }

        assert(valid());
        throw std::bad_alloc();
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type s) {
        //throw an invalid_argument exception, if p is invalid
        int * ptr = reinterpret_cast<int*>(p);
        /*
        make into free block and combine with neighboring free blocks if needed*/


        int totalSize = 0;
        --ptr;


        if(*ptr >= 0 || *ptr *-1 < s*sizeof(T)) {
            std::cout << *ptr << std::endl;
            std::cout << s * sizeof(T) * -1 << std::endl;
            throw std::invalid_argument("exception");
        }

        int numIter = s;

        int * beginPtr = ptr;

        if(begin() != iterator(&(ptr)[0])) {

            beginPtr--;
            if(*beginPtr > 0) {
                totalSize+=(*beginPtr + 2*sizeof(int));


                beginPtr = beginPtr - (*(ptr-1)/sizeof(int) + 1);

            }
            else {
                beginPtr++;
            }


        }

        int size = *ptr * -1;
        totalSize += size;


        *ptr = size;

        ptr+=(1+size/sizeof(int));
        *ptr = size;

        int * endPtr = ptr;

        ptr++;

        if(end() != iterator(&(ptr)[0])) {
            if(*ptr > 0) {
                totalSize+=(*ptr + 2*sizeof(int));
                endPtr = ptr + (*ptr/sizeof(int) + 1);
            }

        }

        *beginPtr = totalSize;
        *endPtr = totalSize;






        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
