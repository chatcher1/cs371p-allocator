# CS371p: Object-Oriented Programming Allocator Repo

* Name: Caroline Hatcher

* EID: cwh2349

* GitLab ID: chatcher1

* HackerRank ID: carolinewhatcher

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)
120d1d3a223c6d45668e951489067e5264de6104

* GitLab Pipelines: (link to your GitLab CI Pipeline)
https://gitlab.com/chatcher1/cs371p-allocator/-/pipelines

* Estimated completion time: 20

* Actual completion time: (actual time in hours, int or float)
20

* Comments: (any additional comments you have)
