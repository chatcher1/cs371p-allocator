// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    string s;
    getline(cin, s);
    int numTests = stoi(s);

    getline(cin, s);
    while (numTests > 0) {
        //    using allocator_type = my_allocator<int, 1000>;
        //     using value_type     = typename allocator_type::value_type;
        //     using size_type      = typename allocator_type::size_type;
        //     using pointer        = typename allocator_type::pointer;

        using allocator_type = my_allocator<double, 1000>;
        using value_type     = typename allocator_type::value_type;
        using size_type      = typename allocator_type::size_type;
        using pointer        = typename allocator_type::pointer;


        size_type st;

        allocator_type x;

        pointer prev = nullptr;
        while (getline(cin, s)) {
            if (s == "" || s == "\n") {
                break;
            }

            int request = stoi(s);
            if(request < 0) {
                //free the block requeted
                // int pos = -1 * request;
                // int ind = 0;
                // while(pos > 1){
                //     pos--;
                //     int sizeBlock = x[ind];
                //     ind += (sizeBlock*sizeof(double) + 2*sizeof(int));
                // }
                int * ptr = reinterpret_cast<int *>(prev)-1;
                while(request <= -1) {
                    //cout << request << " " << *ptr << endl;
                    while(*ptr > 0) {

                        int val = *ptr;
                        if(val < 0) {
                            val*=-1;
                        }
                        ptr = ptr + val/sizeof(int) + 2;
                    }
                    if(request < -1) {
                        int val = *ptr;
                        if(val < 0) {
                            val*=-1;
                        }
                        ptr = ptr + val/sizeof(int) + 2;
                    }
                    request++;
                }
                //cout << *ptr << endl;
                pointer arg = reinterpret_cast<double *>(ptr+1);
                x.deallocate(arg, -1*request);

            }
            else {

                if(prev == nullptr) {
                    prev = x.allocate(request);
                }
                else {
                    pointer l = x.allocate(request);

                }

            }


        }
        int ind = 0;
        while(ind < 1000) {

            int sizeBlock = *reinterpret_cast<int *>(&x[ind]);

            if(sizeBlock < 0) {


                ind += (-1*sizeBlock + 2*sizeof(int));
            }
            else {
                ind += (sizeBlock + 2*sizeof(int));
            }
            cout << sizeBlock;
            if(ind != 1000) {
                cout << " ";
            }
        }
        cout << endl;



        // write out all sentinels
        numTests--;
    }
    // cout << "-40 944"     << endl;
    // cout << "-40 -24 912" << endl;
    // cout << "40 -24 912"  << endl;
    // cout << "72 -24 880"  << endl;
    return 0;
}
